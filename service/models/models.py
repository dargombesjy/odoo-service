# -*- coding: utf-8 -*-
from datetime import timedelta
from odoo import models, fields, api


class ServiceOrder(models.Model):
    _name = 'service.order'
    _description = "Service Orders"

    no_plat = fields.Char(string="Reference", required=True)
    description = fields.Char(string="Description")
    order_date = fields.Date(default=fields.Date.today)

    customer_id = fields.Many2one('res_partner', string="Customer",
                                 domain=[('customer', '=', True)])
    insurance_id = fields.Many2one('res_partner', string="Insurance",
                                  domain=[('insurance', '=', True)])
    responsible_id = fields.Many2one('res_users', ondelete='set_null',
                                     string="Responseble", index=True)
    orderitem_ids = fields.One2many('service.orderitem', 'order_id',
                                    string="Order items")


class OrderItems(models.Model):
    _name = 'service.orderitem'
    _description = "Service Order items"

    num = fields.Char()
    desc = fields.Char()
    type = fields.Selection(
        selection=[('jasa', "Jasa"), ('part', "Sparepart")],
        string="Jasa / Part"
    )
    qty = fields.Integer()
    price = fields.Integer(string="Harga")
    subtotal = fields.Integer(string="Subtotal", compute='_item_subtotal',
                              store=True)
    flat_rate = fields.Float(digits=(0, 2), help="Flate Rate")
    start_date = fields.Date()
    duration = fields.Float(digits=(0, 2), help="Actual duration")
    end_date = fields.Date(string="End Date", compute='_get_end_date',
                           inverse='set_end_date', store=True)
    order_id = fields.Many2one('service.order', ondelete='cascade',
                               string="Order", required=True)

    @api.depends('start_date', 'duration')
    def _get_end_date(self):
        for r in self:
            if not (r.start_date and r.duration):
                r.end_date = r.start_date
                continue

            # Add duration to start_date, but: Monday + 5 days = Saturday
            # so substract one second to get on Friday instead
            duration = timedelta(days=r.duration, seconds=-1)
            r.end_date = r.start_date + duration

    def _set_end_date(self):
        for r in self:
            if not (r.start_date and r.end_date):
                continue

            # Compute the difference between dates, but: Friday - Monday = 4
            # days, so add one day to get 5 days instead
            r.duration = (r.start_date - r.end_date).days + 1

    @api.onchange('it_qty')
    def _verify_qty(self):
        if self.it_qty < 0:
            return {
                'warning': {
                    'title': "Incorrect value",
                    'message': "Quantity may not ne negative"
                },
            }

    @api.depends('it_qty', 'it_price')
    def _item_subtotal(self):
        for r in self:
            r.subtotal = r.it_qty * r.it_price
