from odoo import fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    # add new column to res.partner, by default partners are not insurance
    customer = fields.Boolean("Customer", default=False)
    insurance = fields.Boolean("Insurance", default=False)
    order_ids = fields.Many2many('service.order', string="Orders created",
                                 readonly=True)
